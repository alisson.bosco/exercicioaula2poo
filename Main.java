import src.veiculos.Caminhao;
import src.veiculos.Carro;
import src.veiculos.Moto;
import src.veiculos.Veiculo;

public class Main {

    public static void main( String[] args ) {
        Veiculo carro = new Carro( "Vermelho", 4, "Sedan", "123456789" );
        Veiculo moto = new Moto( "Azul", 2, "Esportiva", "987654321" );
        Veiculo caminhao = new Caminhao( "Branco", 6, "Baú", "456789123" );

        System.out.println( "Chassi do " + carro.getTipo() + ": " + carro.getChassi() );
        System.out.println( "Chassi da " + moto.getTipo() + ": " + moto.getChassi() );
        System.out.println( "Chassi do " + caminhao.getTipo() + ": " + caminhao.getChassi() );
    }
}