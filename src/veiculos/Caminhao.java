package src.veiculos;

public class Caminhao extends Veiculo {

    public Caminhao( String cor, int quantidadeDeRodas, String carroceria, String chassi ) {
        super( cor, quantidadeDeRodas, carroceria, chassi );
    }

    @Override
    public String getTipo() {
        return "Caminhão";
    }
}