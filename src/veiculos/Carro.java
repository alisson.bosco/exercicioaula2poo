package src.veiculos;

public class Carro extends Veiculo {

    public Carro( String cor, int quantidadeDeRodas, String carroceria, String chassi ) {
        super( cor, quantidadeDeRodas, carroceria, chassi );
    }

    @Override
    public String getTipo() {
        return "Carro";
    }
}
