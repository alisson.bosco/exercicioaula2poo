package src.veiculos;

public class Moto extends Veiculo {

    public Moto( String cor, int quantidadeDeRodas, String carroceria, String chassi ) {
        super( cor, quantidadeDeRodas, carroceria, chassi );
    }

    @Override
    public String getTipo() {
        return "Moto";
    }
}