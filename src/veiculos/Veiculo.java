package src.veiculos;

public class Veiculo {

    private String cor;
    private int quantidadeDeRodas;
    private String carroceria;
    private String chassi;

    public Veiculo( String cor, int quantidadeDeRodas, String carroceria, String chassi ) {
        this.cor = cor;
        this.quantidadeDeRodas = quantidadeDeRodas;
        this.carroceria = carroceria;
        this.chassi = chassi;
    }

    public String getChassi() {
        return chassi;
    }

    public String getTipo() {
        return "Veículo";
    }
}
